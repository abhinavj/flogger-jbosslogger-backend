Usage

1-> add dependency in project

##
    <dependency>
      <groupId>org.example</groupId>
      <artifactId>Flogger-JbossLogger-backend</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
##

2-> set System Properties

by code
##
    System.setProperty("flogger.backend_factory", "com.flogger.jbosslogger.backend.JBossBackendFactory#getInstance");
##
by cli
##
    java -Dflogger.backend_factory=com.flogger.jbosslogger.backend.JBossBackendFactory#getInstance -jar target/<runner-jar>
##
for quarkus dev mode
##
    ./mvnw compile quarkus:dev -Dflogger.backend_factory=com.flogger.jbosslogger.backend.JBossBackendFactory#getInstance
##

3-> Create FluentLogger

##
    private static final FluentLogger logger = FluentLogger.forEnclosingClass();
##
