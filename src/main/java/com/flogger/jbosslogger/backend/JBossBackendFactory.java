package com.flogger.jbosslogger.backend;

import com.google.common.flogger.backend.LoggerBackend;
import com.google.common.flogger.backend.system.BackendFactory;
import org.jboss.logging.Logger;


public class JBossBackendFactory extends BackendFactory {

    private static final JBossBackendFactory INSTANCE = new JBossBackendFactory();

    private JBossBackendFactory() {}

    public static BackendFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public LoggerBackend create(String loggingClassName) {
        return new JBossBackend(Logger.getLogger(loggingClassName));
    }
}
