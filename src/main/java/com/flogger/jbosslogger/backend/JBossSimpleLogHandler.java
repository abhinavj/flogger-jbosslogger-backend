package com.flogger.jbosslogger.backend;

import com.google.common.flogger.backend.SimpleMessageFormatter.SimpleLogHandler;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.logging.Level;


/** Class that represents a log entry that can be written to JBossLogger. */
final class JBossSimpleLogHandler implements SimpleLogHandler {

    private final Map<String, Object> out;

    JBossSimpleLogHandler(Map<String, Object> out) {
        this.out = out;
    }

    @Override
    public void handleFormattedLogMessage(Level level, String message, Throwable thrown) {
        if (level != null) out.put("level", JBossBackend.toJBossLogLevel(level));
        if (message != null) {
            if (!message.startsWith("[CONTEXT")) {
                if (message.endsWith("]")) message = message.split(" \\[CONTEXT")[0];
                if (!message.isEmpty()) out.put("message", message);
            }
        }
        if (thrown != null) {
            out.put("thrown", thrown);
            try (StringWriter stringWriter = new StringWriter()) {
                try (PrintWriter printWriter = new PrintWriter(stringWriter)) {
                    thrown.printStackTrace(printWriter);
                    out.put("stackTrace", stringWriter.toString());
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }
}
