import com.google.common.flogger.FluentLogger;

public class LogUtil {

    static {
        System.setProperty("flogger.backend_factory", "com.flogger.jbosslogger.backend.JBossBackendFactory#getInstance");
        System.setProperty("flogger.exclusive", "true");
//        System.setProperty("flogger.FINER", "true");
//        System.setProperty("flogger.level","500");
    }

    private static final FluentLogger logger = FluentLogger.forEnclosingClass();

    public void logData(String data) {
        logger.atInfo().log(data + " INFO");
        logger.atConfig().log(data + " CONFIG");
        logger.atWarning().log(data + " WARNING");
        logger.atSevere().log(data + " SEVERE");
        logger.atFine().log(data + " FINE");
        logger.atFiner().log(data + " FINER");
        logger.atFinest().log(data + " FINEST");
//        logger.atInfo().log("SEVERE " + String.valueOf(logger.atSevere().isEnabled()));
//        logger.atInfo().log( "WARNING " + String.valueOf(logger.atWarning().isEnabled()));
//        logger.atInfo().log("INFO " + String.valueOf(logger.atInfo().isEnabled()));
//        logger.atInfo().log("CONFIG " + String.valueOf(logger.atConfig().isEnabled()));
//        logger.atInfo().log("FINE " + String.valueOf(logger.atFine().isEnabled()));
//        logger.atInfo().log("FINER " + String.valueOf(logger.atFiner().isEnabled()));
//        logger.atInfo().log("FINEST " + String.valueOf(logger.atFinest().isEnabled()));

    }

    public void logLevelAndCode() {
        System.out.println(
                " SEVERE :1000"
                        + " WARNING : 900"
                        + " INFO :800"
                        + " CONFIG: 700 "
                        + " FINE: 500 "
                        + " FINER: 400 "
                        + " FINEST: 300");
        /*
        "|  OFF  | SEVERE | WARNING | INFO, CONFIG | FINE, FINER, FINEST | ALL |
 | FATAL | ----- | ERROR  | WARN,   | INFO, ------ |       DEBUG,        | --- | TRACE |
 */
    }
}